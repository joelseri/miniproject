<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        // 
        for ($i = 0; $i < 31; $i++) {
            $category = new category();
            $category->setName($faker->name);
           
      
        $manager->persist($category);
        $manager->flush();
    }
}
}