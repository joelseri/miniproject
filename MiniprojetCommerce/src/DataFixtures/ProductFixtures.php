<?php

namespace App\DataFixtures;
use Faker\Factory;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        // On configure dans quelles langues nous voulons nos données
        $faker = Factory::create('fr_FR');

        // on créé 99 produits
        for ($i = 0; $i < 100; $i++) {
            $product = new Product();
            $product->setName($faker->words(2, true));
            $product->setPrice($faker->numberBetween(20, 500));
            $product->setDescription($faker->sentences(3, true));
            $product->setSold(false);
      
            $manager->persist($product);
        }
        $manager->flush();
    }

}
