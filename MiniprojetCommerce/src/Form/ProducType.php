<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Vich\UploaderBundle\Form\Type\VichFileType;

class ProducType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, [
                    'label' => 'Nom du Produit'
                ])
                ->add('description')
                ->add('price', MoneyType::class, [
                    'label' => 'Prix du produit',
                    
                ])
                ->add('imageFile', \Symfony\Component\Form\Extension\Core\Type\FileType::class,[
                    'label'=>'Image du produit',
                    'required'=>false
                ])
                ->add('sold')
                ->add('categories', EntityType::class,[
                    'class'=>Category::class,
                    'choice_label'=>'name',
                    'required'=>false,
                    'multiple' =>true
                    
                ])
                ->add('created_at', \Symfony\Component\Form\Extension\Core\Type\DateType::class, [
                    'widget'=>'single_text',
                    'label'=>'Date',
                ])
                
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

}
