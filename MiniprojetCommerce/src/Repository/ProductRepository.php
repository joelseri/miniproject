<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use \Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use App\Entity\ProductSearch;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Product::class);
    }

    /**
     * 
     * @return Query
     */
    public function findAllVisibleQuery(ProductSearch $search): Query {

        $query = $this->findVisibleQuery();

        if ($search->getMaxPrice()) {
            $query = $query
                    ->andWhere('p.price <=:maxPrice')
                    ->setParameter('maxPrice', $search->getMaxPrice());
        }

        if ($search->getMinPrice()) {
            $query = $query
                    ->andWhere('p.price >=:minPrice')
                    ->setParameter('minPrice', $search->getMinPrice());
        }

        if ($search->getCategories()) {

            foreach ($search->getCategories() as $key => $category) {

                $query = $query
                        ->andWhere(":category$key MEMBER OF p.categories")
                        ->setParameter("category$key", $category);
            }
        }

        return $query->getQuery();
    }

    /**
     * @return Product[]
     */
    public function findLatest(): array {
        return $this->findVisibleQuery()
                        ->getQuery()
                        ->getResult();
    }

    private function findVisibleQuery(): QueryBuilder {
        return $this->createQueryBuilder('p')
                        ->setMaxResults(4)
                        ->where('p.sold = 0')
                        ->orderBy('p.id', 'Desc');
    }
/**
 * 
 * @param type $array
 * @return Product[]
 * 
 */
    public function findArray($array):array {
        $qb = $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.id IN (:array)')
                ->setParameter('array', $array);
        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('p')
      ->andWhere('p.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('p.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?Product
      {
      return $this->createQueryBuilder('p')
      ->andWhere('p.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
