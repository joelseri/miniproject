<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductSearchType;
use App\Entity\ProductSearch;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductController
 *
 * @author Administrateur
 */
class ProductController extends AbstractController {

    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @@var ObjectManager $em
     */
    private $em;

    public function __construct(ProductRepository $repository, ObjectManager $em) {
        $this->repository = $repository;
        $this->em         = $em;
    }

    /**
     * 
     * @return Response
     * @Route("/product", name="product_index")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response {

        $user              = $this->getUser();
        $search            = new ProductSearch();
        $productSearchForm = $this->createForm(ProductSearchType::class, $search);
        $productSearchForm->handleRequest($request);

        $products = $paginator->paginate(
                $this->repository->findAllVisibleQuery($search), $request->query->getInt('page', 1), 10);

      


        return $this->render('index.html.twig', [
                    'current_menu' => 'product',
                    'products'     => $products,
                    'user'         => $user,
                    'search_form'  => $productSearchForm->createView(),
        ]);
    }

    /**
     * @Route("/product/{slug}.{id}", name="product_show", requirements={"slug": "[a-z0-9\-]*"})
     * @return Response
     */
    public function show(Product $product, string $slug): Response {
       
        $user = $this->getUser();
        if ($product->getSlug() != $slug) {

            return $this->redirectToRoute('product_show', [
                        'id'   => $product->getId(),
                        
                        'slug' => $product->getSlug()
                            ], 301);
        }

       
        return $this->render('show.html.twig', [
                    'product' => $product,
                    'user' => $user,
                    'current' => 'products'
        ]);
    }

}
