<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HomePageController
 *
 * @author joel
 */


class HomePageController extends AbstractController {

    
      /**
     * @var ProductRepository
     */
    private  $repository;
    
    /**
     * @@var ObjectManager $em
     */
    
    private $em;
    
    public function __construct(ProductRepository $repository,ObjectManager $em ) {
       $this->repository = $repository;
       $this->em         = $em;
    }
    /**         
     * @return Response
     * @Route("/", name="home")
     */
    public function index(ProductRepository $repository): Response
    {
        $user    = $this->getUser();
        $product = $repository->findLatest();
        return $this->render('home.html.twig', [
                    'products' => $product,
                    'user'     => $user
        ]);
    }

}
