<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Description of shoppingController
 *
 * @author joel
 */
class ShoppingController extends AbstractController {

     /**
     * @param Request $request
     * @return Response
     */
    public  function nbArticle(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('panier'))
            $articles = 0;
        else
            $articles = count($session->get('panier'));
        return $this->render('ShoppingCart/test.html.twig',[
            "articles" => $articles
        ]);
    }
    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository) {
        $this->repository = $repository;
    }
    
    

    /**
     * 
     * @param int $id
     * @return Response
     * @Route("/panier/{id}", name="panier_add")
     */
    public function ShoppingCartAdd(Request $request, $id) {
        $session = $request->getSession();

        if (!$session->has('panier')) {
            $session->set('panier', array());
            $panier = $session->get('panier');
        }


        //$panier[ID DU PRODUIT] => QUANTITE
        $panier = $session->get('panier');
        if (array_key_exists('id'  , $panier)) {

            if ($request()->query->get('qte') != null) {
                $panier[$id] = $request->query->get('qte');
                $this->get('session')->getFlashBag()->add('success', 'Quantité modifié avec succés');
            }
        } else {

            if ($request->query->get('qte') != null) {
                $panier[$id] = $request->query->get('qte');
            } else {
                $panier[$id] =1;
            }


            $this->get('session')->getFlashBag()->add('success', 'Article ajouté avec succés');
        }

        $session->set('panier', $panier);


        return $this->redirect($this->generateUrl('panier'));
    }

    /**
     * 
     * @param Request $request
     * @return Response
     * @Route("/panier", name="panier")
     */
     public function index(Request $request): Response
    {
        $user =$this->getUser();
        $session = $request->getSession();
        $product = $this->repository->findArray(array_keys($session->get('panier')));
        if (!$session->has('panier'))
        {
            $session->set('panier', array());
        }
        return $this->render('ShoppingCart/index.html.twig',[
            "products" => $product,
            "user" =>$user,
            "panier" => $session->get('panier')
        ]);
    }

    
      /**
     * @Route("/delete/{id}", name="panier_suppArticle")
     * @param SessionInterface $session
     * @param $id
     * @return Response
     */
    public function supprimerProduct(SessionInterface $session, $id)
    {
        $panier = $session->get('panier');
        if (array_key_exists($id, $panier))
        {
            unset($panier[$id]);
            $session->set('panier', $panier);
            $this->addFlash('danger', 'Article supprimé avec succès !');
        }
        return $this->redirect($this->generateUrl('panier'));
    }
    
     /**
     * @Route("/delete", name="panier_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteProduct(Request $request): Response
    {
        $session = $request->getSession();
        $session->clear();
        $session->set('panier', array());
        return $this->redirect($this->generateUrl('panier'));
    }

}
