<?php

NameSpace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Product;
//use App\Entity\Category;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ProducType;
use Symfony\Component\HttpFoundation\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminController
 *
 * @author joel
 */
class AdminController extends AbstractController {

    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @@var ObjectManager $em
     */
    private $em;

    public function __construct(ProductRepository $repository, ObjectManager $em) {
        $this->repository = $repository;
        $this->em         = $em;
    }

    /**
     * @Route("/admin", name="admin_product_index")
     * @return Response
     * 
     */
    public function indexProduct() {
        $user     = $this->getUser();
        $products = $this->repository->findAll();
        return $this->render('Admin/Product/index.html.twig', [
        'products' =>$products,
        'user' => $user
        
        ]);
    }

    /**
     * @Route("/admin/product/{id}", name="admin_product_edit" ,methods={"GET","POST"})
     * @return Response
     * 
     */
    public function editProduct(Product $product, Request $request): Response {
        
       // $category = new Category();
       // $product->addCategory($category);
        $form = $this->createForm(ProducType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Bien modifier avec succès');
            return $this->redirectToRoute('admin_product_index');
        }
        return $this->render('Admin/Product/edit.html.twig', [
                    'product' => $product,
                    'form'    => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/create", name="admin_product_new")
     * @return Response
     * 
     */
    public function newProduct(Request $request) {

        $product = new Product();
        $form    = $this->createForm(ProducType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($product);
            $this->em->flush();
            $this->addFlash('success', 'Bien ajouter avec succès');
            return $this->redirectToRoute('admin_product_index');
        }
        return $this->render('Admin/Product/new.html.twig', [
                    'product' => $product,
                    'form'    => $form->createView()
        ]);
    }

    /**
     * @Route("admin/product/{id}", name="admin_product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product) {
        if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
            $this->addFlash('success', 'Bien supprimer avec succès');
        }

        return $this->redirectToRoute('admin_product_index');
    }

}
