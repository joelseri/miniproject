<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductSearchType;
use App\Entity\ProductSearch;

class ProductSearchController extends AbstractController
{
    /**
     * @Route("/product/search", name="product_search")
     */
    public function ProductSearch(Request $request)
    {
        $search = new ProductSearch();
        $productSearchForm = $this->createForm(ProductSearchType::class,$search);
        $productSearchForm->handleRequest($request);
        return $this->render('product_search/index.html.twig', [
            'controller_name' => 'Recherche de produits',
             'search_form'=>$productSearchForm->createView(),
            'current_menu' => 'search'
        ]);
    }
}
