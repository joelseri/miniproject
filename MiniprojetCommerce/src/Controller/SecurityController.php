<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Description of SecurityController
 *
 * @author Administrateur
 */
class SecurityController extends AbstractController {

    /**
     * @Route("/login", name="login", methods={"GET","POST"})
     */
    public function login(){
        /* $error = $authentificationUtils->getLastAuthenticationError();
        $lastUsername = $authentificationUtils->getLastUsername(); */
        return $this->render('security/login.html.twig', [
            
           
        ]);
    }
    /**
     * @Route("/registration", name="security_registration")
     *
     */
    public function Registration(Request $request, ObjectManager $manager, 
    UserPasswordEncoderInterface $encoder){
        $user = new User();
        //on reli les champs de l'utilisateur au formulaire
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
        
            $user->setPassword($hash);
           $manager->persist($user);
           $manager->flush();
             $this->addFlash('success', 'Bien modifier avec succès');
            return $this->redirectToRoute('login');
        }
        return $this->render('security/registration.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        
    }
}
