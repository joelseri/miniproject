<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *  @UniqueEntity("name")
 * @Vich\Uploadable
 */
class Product {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     * @var File|null
     * 
     */
    private $imageFile;

   

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1, max=10000)
     */
    private $price;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $sold = false;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime|null
     */
    private $created_at;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="products")
     */
    private $categories;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

   
   

    public function __construct() {
        $this->created_at = new \DateTime;
     
        $this->categories = new ArrayCollection();
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getSlug(): String {
        return (new Slugify())->slugify($this->name);
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription(string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice(int $price): self {
        $this->price = $price;

        return $this;
    }

    public function getFormattedPrice(): String {
        return number_format($this->price, 0, '', '');
    }

    public function getSold() {
        return $this->sold;
    }

    public function setSold(bool $sold): self {
        $this->sold = $sold;

        return $this;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection {
        return $this->categories;
    }

    public function addCategory(Category $category): self {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeProduct($this);
        }

        return $this;
    }

    function getImageFile() {
        return $this->imageFile;
    }

    
    function getImageName() {
        return $this->imageName;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    /**
     * 
     * @param File $imageFile
     */
    function setImageFile(File $imageFile = null) {
        $this->imageFile = $imageFile;
        if ($this->imageFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

  
    function setImageName($imageName) {
        $this->imageName = $imageName;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

   

   

}
