<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of ProductSearch
 *
 * @author Administrateur
 */
class ProductSearch {

    /**
     *
     * @var int|null 
     */
    private $minPrice;

    /**
     *
     * @var int|null
     */
    private $maxPrice;

    /**
     * @var ArrayCollection
     */
    private $categories;

    public function __construct() {
        $this->category = new ArrayCollection();
    }

    function getMinPrice() {
        return $this->minPrice;
    }

    function getMaxPrice() {
        return $this->maxPrice;
    }

    /**
     * 
     * @var|null int $minPrice
     */
    function setMinPrice(int $minPrice) {
        $this->minPrice = $minPrice;
    }

    /**
     * 
     * @var int|null $maxPrice
     */
    function setMaxPrice(int $maxPrice) {
        $this->maxPrice = $maxPrice;
    }
    
    /**
     * 
     * @return ArrayCollection
     */
    function getCategories() {
        return $this->categories;
    }

    /**
     * 
     * @param ArrayCollection $categories
     * 
     */
    function setCategories(ArrayCollection $categories) {
        $this->categories = $categories;
    }



}
