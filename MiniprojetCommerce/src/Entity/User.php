<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 * fields={"Email"}, message="email déja utiliser")
 */
class User implements UserInterface {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 8,
     *      max = 10,
     *      minMessage = "Mot de passe trop court, La longueur minimum est de {{ limit }} ",
     *      maxMessage = "Mot de passe trop long, La longueur maximum est de {{ limit }} "
     * )
     
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="votre mot de passe ne sont pas identique")
     */
    public $confirm_password;

     public function getConfirmPassword()
    {
        return $this->confirm_password;
    }

    public function setConfirmPassword(string $confirm_password)
    {
        $this->confirm_password = $confirm_password;
    }
    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true, nullable=true)
     * @Assert\Email()
     */
    private $Email;

   

    public function getId(): int {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }
    

    public function setUsername(string $username): self {
        $this->username = $username;

        return $this;
    }
    
    /**
     * 
     * @return string password
     */

    public function getPassword() {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }
    

    /**
    * 
    * @return (Role|string)[] the user roles
    */
        public function getRoles() {
        return ['ROLE_ADMIN'];
        }

    /**
     * @return string|null The salt
     */
    public function getSalt() {
        return null;
    }

    public function eraseCredentials() {
        
    }

    /* public function serialize() {
        return $this->serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->Email,
        ]);
    } */
   /*  public function unserialize($serialized) {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->Email   
           ) = unserialize($serialized, ['allow_classes' => false]);
    } */

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }


}
